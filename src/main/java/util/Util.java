package util;

import model.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class Util {
    private static SessionFactory sessionFactory = createConfiguratiun().buildSessionFactory();

    public static org.hibernate.SessionFactory getSessionFactory() {
        return sessionFactory;

    }


    public static Configuration createConfiguratiun() {
        Configuration config = new Configuration();
        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://127.0.0.1:3306/rent_a_bike");
        settings.put(Environment.USER, "root");
        settings.put(Environment.PASS, "Andreea1996");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        config.setProperties(settings);
        config.addAnnotatedClass(User.class);
        config.addAnnotatedClass(Vehicle.class);
        config.addAnnotatedClass(Location.class);
        config.addAnnotatedClass(Payment.class);
        config.addAnnotatedClass(Type.class);
        config.addAnnotatedClass(City.class);
        return config;

    }

}
