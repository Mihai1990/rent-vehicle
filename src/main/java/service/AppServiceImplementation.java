package service;

import dao.UserDao;
import dao.UserDaoImpl;
import model.User;

public class AppServiceImplementation implements AppService {
    UserDao userDao = new UserDaoImpl();

    public AppServiceImplementation() {
    }

    @Override
    public boolean authenticate(String name) {
        User user = this.userDao.getUserByName(name);
        System.out.println(user.getCNP());
        if (user.equals(null)){
            return false;
        }else return true;
    }

}
