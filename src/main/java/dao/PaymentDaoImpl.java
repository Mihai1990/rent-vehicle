package dao;

import model.Payment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.Util;

public class PaymentDaoImpl implements PaymentDao{
    SessionFactory sessionFactory= Util.getSessionFactory();


    public Payment createPayment(Payment payment) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        session.save(payment);
        session.close();
        return payment;
    }

    public Payment getPayment(int id) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        Payment payment = session.get(Payment.class,id);
        session.close();
        return payment;
    }

    public void removePayment(int id) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Payment payment = session.get(Payment.class,id);
        session.remove(payment);
        transaction.commit();
        session.close();


    }

    public Payment updatePayment(int id, Payment payment) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Payment payment1 = session.get(Payment.class,id);
        payment1.setBeginDate(payment.getBeginDate());
        payment1.setEndDate(payment.getEndDate());
        session.update(payment1);
        transaction.commit();
        session.close();
        return payment1;

    }
}
