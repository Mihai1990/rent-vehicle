package dao;

import model.Location;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.Util;

public class LocationDaoImpl implements LocationDao {

    SessionFactory sessionFactory = Util.getSessionFactory();

    public Location createLocation(Location location) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        session.save(location);
        session.close();
        return location;

    }

    public Location getLocation(int id) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Location location = session.get(Location.class, id);
        session.close();
        return location;

    }

    public void removeLocation(int id) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Location location = session.get(Location.class, id);
        session.remove(location);
        transaction.commit();
        session.close();

    }

    public Location updateLocation(int id, Location location) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Location location1 = session.get(Location.class, id);
        location1.setName(location.getName());
        session.update(location1);
        transaction.commit();
        session.close();
        return location1;
    }
}
