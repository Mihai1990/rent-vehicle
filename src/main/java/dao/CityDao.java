package dao;

import model.City;

public interface CityDao {
    City addCity (City city);
    City getCity (int id);
    void deleteCity (int id);
    City updateCity (int id,City city);

}
