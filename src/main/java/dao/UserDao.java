package dao;

import model.User;

public interface UserDao {

    User addUser(User user);
    User getUser (int id);
    void deleteUser (int id);
    User updateUser(int id,User user);
    User getUserByName(String name);
}
