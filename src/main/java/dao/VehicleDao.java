package dao;

import model.Vehicle;

public interface VehicleDao {
    Vehicle createVehicle(Vehicle vehicle);
    Vehicle getVehicle (int id);
    void removeVehicle(int id);
    Vehicle updateVehicle (int id, Vehicle vehicle);
}
