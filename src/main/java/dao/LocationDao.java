package dao;

import model.Location;

public interface LocationDao {



    Location createLocation(Location location);
    Location getLocation (int id);
    void removeLocation(int id);
    Location updateLocation(int id,Location location);
}
