package dao;

import model.Location;
import model.Payment;
import model.Type;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.Util;

public class TypeDaoImpl implements TypeDao {
    SessionFactory sessionFactory = Util.getSessionFactory();

    public Type addType(Type type) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        session.save(type);
        session.close();
        return type;
    }

    public Type getType(int id) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Type type = session.get(Type.class, id);
        session.close();
        return type;
    }

    public void deleteType(int id) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Type type = session.get(Type.class, id);
        session.remove(type);
        transaction.commit();
        session.close();

    }

    public Type updateType(int id, Type type) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Type type1 = session.get(Type.class, id);
       type1.setName(type.getName());
        type1.setDescription(type.getDescription());
        session.update(type1);
        transaction.commit();
        session.close();
        return type1;
    }
}
