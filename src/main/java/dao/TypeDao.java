package dao;

import model.Type;

public interface TypeDao {
    Type addType (Type type);
    Type getType (int id);
    void deleteType (int id);
    Type updateType (int id, Type type);
}
