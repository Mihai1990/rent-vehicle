package dao;

import model.User;
import model.Vehicle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.Util;

public class UserDaoImpl implements UserDao {
    SessionFactory sessionFactory = Util.getSessionFactory();

    public User addUser(User user) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        session.save(user);
        session.close();
        return user;


    }

    public User getUser(int id) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        User user = session.get(User.class, id);
        session.close();
        return user;
    }

    @Override
    public User getUserByName(String name) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        String hql = "FROM User as U WHERE U.name = '" + name + "'";
        User user = (User) session.createQuery(hql).getSingleResult();
        return user;

    }

    public void deleteUser(int id) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        User user = session.get(User.class, id);
        session.remove(user);
        transaction.commit();
        session.close();

    }

    public User updateUser(int id, User user) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        User user1 = session.get(User.class, id);
        user1.setAdress(user.getAdress());
        user1.setName(user.getName());
        user1.setCNP(user.getCNP());
        user1.setEmail(user.getEmail());
        session.update(user1);
        transaction.commit();
        session.close();
        return user1;


    }

}
