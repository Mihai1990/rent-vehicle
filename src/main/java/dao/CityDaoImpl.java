package dao;

import model.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.Util;

public class CityDaoImpl implements CityDao {

    SessionFactory sessionFactory = Util.getSessionFactory();
    public CityDaoImpl() {

    }

    public City addCity(City city) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        session.save(city);
        session.close();
        return city;

    }

    public City getCity(int id) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        City city = session.get(City.class, id);
        session.close();
        return city;

    }

    public void deleteCity(int id) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        City city = session.get(City.class, id);
        session.remove(city);
        transaction.commit();
        session.close();

    }

    public City updateCity(int id, City city) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        City pCity = session.get(City.class, id);
        pCity.setName(city.getName());
        session.update(pCity);
        transaction.commit();
        session.close();
        return pCity;
    }
}
