package dao;

import model.Payment;

public interface PaymentDao {


    Payment createPayment(Payment payment);
    Payment getPayment(int id);
    void removePayment(int id);
    Payment updatePayment(int id, Payment payment);
}
