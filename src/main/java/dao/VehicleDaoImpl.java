package dao;


import model.Payment;
import model.Vehicle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.Util;

public class VehicleDaoImpl implements VehicleDao {
    SessionFactory sessionFactory = Util.getSessionFactory();

    public Vehicle createVehicle(Vehicle vehicle) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        session.save(vehicle);
        session.close();
        return vehicle;
    }

    public Vehicle getVehicle(int id) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        Vehicle vehicle = session.get(Vehicle.class,id);
        session.close();
        return vehicle;
    }

    public void removeVehicle(int id) {
        sessionFactory.openSession();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Vehicle vehicle = session.get(Vehicle.class,id);
        session.remove(vehicle);
        transaction.commit();
        session.close();

    }

    public Vehicle updateVehicle(int id, Vehicle vehicle) {
        sessionFactory.openSession();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Vehicle vehicle1 = session.get(Vehicle.class,id);
        vehicle1.setBrand(vehicle.getBrand());
        vehicle1.setColor(vehicle.getColor());
        vehicle1.setSize(vehicle.getSize());
        session.update(vehicle1);
        transaction.commit();
        session.close();
        return vehicle1;
    }
}
