package javafx;

import javafx.application.Application;
import javafx.stage.Stage;

public class JavaFxMain  extends Application {
    public void start(Stage stage) throws Exception {
        stage.setHeight(500);
        stage.setWidth(500);
        stage.setX(500);
        stage.setY(200);
        stage.setTitle("RENT APP");
    }
}
