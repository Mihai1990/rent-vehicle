package javafx;

import javafx.application.Application;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import service.AppService;
import service.AppServiceImplementation;


public class Main extends Application {
    public void start(Stage stage) throws Exception {
        stage.setHeight(500);
        stage.setWidth(500);
        stage.setX(500);
        stage.setY(200);
        stage.setTitle("RENT APP");


        Label welcomeLabel = new Label("Welcome to RENT APP.");


        VBox buttonVBox = new VBox();
        buttonVBox.setSpacing(15);
        buttonVBox.setAlignment(Pos.CENTER);


        Label chooseCity = new Label("Please choose a city: ");

        ComboBox<String> citiesComboBox = new ComboBox<>();
        citiesComboBox.getItems().addAll("", "Cluj-Napoca", "Suceava", "Oradea", "Bucuresti");
        citiesComboBox.getSelectionModel().select(0);

        Label chooseLocation = new Label("Please choose a location in your city: ");

        ComboBox<String> locationComboBox = new ComboBox<>();

        locationComboBox.getItems().addAll("", "Piata Avram Iancu", "Piata Mihai Viteazul", "Piata sfatului");
        locationComboBox.getSelectionModel().select(0);


        VBox vehicleVBox = new VBox();
        vehicleVBox.setAlignment(Pos.CENTER);
        vehicleVBox.setSpacing(10);

        Label chooseTypeLabel = new Label("Please choose a vehicle type: ");
        ComboBox<String> typeVehicleComboBox = new ComboBox<>();
        typeVehicleComboBox.getItems().addAll("", "Bicycle", "Scooters", "Skateboards", "Hoverboards", "Rolls");
        typeVehicleComboBox.getSelectionModel().select(0);

        VBox productVBox = new VBox();
        productVBox.setAlignment(Pos.CENTER);
        productVBox.setSpacing(10);

        Label choseProduct = new Label("Choose the best product for you");

        ListView<String> vehicleList = new ListView<>();
        vehicleList.getItems().addAll("Bmx", "Montain Bike", "Pegas", "B MAX");

        VBox paymentVBox = new VBox();
        paymentVBox.setAlignment(Pos.CENTER);
        paymentVBox.setSpacing(10);


        Scene scene5 = new Scene(paymentVBox);
        Button nextScene5 = new Button("NEXT");
        nextScene5.setOnMouseClicked(event -> stage.setScene(scene5));

        HBox selectPaymentCash = new HBox();
        selectPaymentCash.setAlignment(Pos.CENTER);
        selectPaymentCash.setSpacing(10);

        HBox selectPaymentCard = new HBox();
        selectPaymentCard.setAlignment(Pos.CENTER);
        selectPaymentCard.setSpacing(10);


        Label paymentLabel = new Label("Select the payment method");

        Scene scene6 = new Scene(selectPaymentCash);
        Button nextScene6 = new Button("CASH");
        nextScene6.setPrefWidth(200);
        nextScene6.setPrefHeight(40);
        nextScene6.setOnMouseClicked(event -> stage.setScene(scene6));

        Scene scene7 = new Scene(selectPaymentCard);
        Button nextScene7 = new Button("CARD");
        nextScene7.setPrefWidth(200);
        nextScene7.setPrefHeight(40);

        nextScene7.setOnMouseClicked(event -> stage.setScene(scene7));
        paymentVBox.getChildren().addAll(paymentLabel, nextScene6, nextScene7);

        productVBox.getChildren().addAll(choseProduct, vehicleList, nextScene5);

        Scene scene4 = new Scene(productVBox);
        Button nextScene4 = new Button("NEXT");
        nextScene4.setOnMouseClicked(event -> stage.setScene(scene4));


        vehicleVBox.getChildren().addAll(chooseTypeLabel, typeVehicleComboBox, nextScene4);


        Scene scene3 = new Scene(vehicleVBox);
        Button nextScene3 = new Button("NEXT");
        nextScene3.setOnMouseClicked(event -> stage.setScene(scene3));

        buttonVBox.getChildren().addAll(chooseCity, citiesComboBox, chooseLocation, locationComboBox, nextScene3);


        Scene secondScene = new Scene(buttonVBox);
        stage.setScene(secondScene);

        Label usernameLabel = new Label("Username: ");
        Label passwordLabel = new Label("Password: ");

        TextField usernameTextField = new TextField();
        usernameTextField.setMaxSize(200, 40);

        PasswordField passwordField = new PasswordField();
        passwordField.setMaxSize(200, 40);

        AppService appService = new AppServiceImplementation();

        Button logButton = new Button("Login");
        logButton.setOnMouseClicked(event ->{
            String userName = usernameTextField.getCharacters().toString();
            appService.authenticate(userName);
        stage.setScene(secondScene);
        }
        );

        VBox loginVBox = new VBox();
        loginVBox.setAlignment(Pos.TOP_CENTER);
        loginVBox.setSpacing(10);
        loginVBox.getChildren().addAll(usernameLabel, usernameTextField, passwordLabel, passwordField, logButton);
        Scene scene = new Scene(loginVBox);


        stage.setScene(scene);
        stage.show();


    }
}
